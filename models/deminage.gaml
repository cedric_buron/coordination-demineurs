/**
* Name: exploration spatiale
* Author: cburon
* Description: Modèle pour le premier TP du cours de l'ENSTA Systèmes MultiAgents
*/

model deminage

global{
	int proba_changement_cap<-20;
	int proba_depot_balise<-5;
	int nb_robots <- 4;
	int nb_mine <- 6;
	int nb_obstacle <- 8;
	float vitesse_robot<-2.0;
	float distance_vue<-8.0;
	init{
		create zone_deploiement number:1;
		create robot number:nb_robots with:[vitesse::2.0];
		create mine number:nb_mine;
		create obstacle number:nb_obstacle;
	}
}

/* Insert your model definition here */

species zone_deploiement{
	aspect base{
		draw circle(4) color:#green;
	}
}

species robot skills:[moving]{
	int tour_balise;
	int angle;
	float vitesse;
	int nb_balise;
	init{
		tour_balise<-0;
		nb_balise<-2;
	}
	bool intersection(list<point> points , float rayon){
		loop p over:points{
			point intersect;
			if(angle=0 or angle=180){
				intersect<-{p.x,self.location.y};
				write(string(self.location) + " - intersection avec " + p + ": "+intersect);
			}
			else if(angle=90 or angle=270){
				intersect<-{self.location.x, p.y};
				write(string(self.location) + " - intersection avec " + p + ": "+intersect);
			}
			else{
				float x <- (tan(angle)*self.location.x-tan(angle-90)*p.x-self.location.y+p.y)/(tan(angle)-tan(angle-90));
				float y <- self.location.y+tan(angle)*(x-self.location.x);
				intersect <- point(x,y);
				write(string(self.location) + " - intersection avec " + p + ": "+intersect);
	//			write(self.name + " - pourcentage" + abs((intersect.x-self.location.x)/(vitesse*cos(angle))) + ";" + abs((intersect.y-self.location.y)/(vitesse*sin(angle))));
			}
			point deplacement <-{self.location.x+vitesse*cos(angle),self.location.y+vitesse*sin(angle)};
			write(self.name + " - pourcentage: " + abs((intersect.x-self.location.x)));
			if((distance_to(self,intersect)+distance_to(intersect,deplacement))>distance_to(self, deplacement)){
				intersect <- deplacement;
				write(self.name + " - exterieur: "+intersect);
			}
			if(distance_to(intersect, p)<rayon){
				return true;
			}
		}
		return false;
	}
//	int randomchgt(int min, int max){
//	}

	int chgt_cap(int angle_initial){
		int tmp;
		int min<-angle_initial-45;
		int max <- angle_initial + 45;
		float total <- 0.0;
		loop i from: min to: max{
			point futur <- {self.location.x + cos(i), self.location.y+sin(i)};
			int angle_vaisseau <-angle_between(self.location,futur,any(zone_deploiement).location);
			if(angle_vaisseau<90 or angle_vaisseau>270){
				total <- total+abs(sin(angle_vaisseau));
			}
			else{
				total<-total+1;
			}
		}
		if(total=0.0){
			tmp <- rnd(min, max);
		}
		else{
			float proba<-rnd(total);
			float cumul<-0.0;
			loop i from:min to:max{
				point futur <- {self.location.x + cos(i), self.location.y+sin(i)};
				int angle_robot <-angle_between(self.location,futur,any(zone_deploiement).location);
				if(angle_robot<90 or angle_robot>270){
					cumul <- cumul+abs(sin(angle_robot));
				}
				else{
					cumul<-cumul+1;
				}
				if(cumul>=proba){
					if(i>360){
						tmp <- int(i-360);
					}
					if(i<0){
						tmp <- int(i+360);
					}
					tmp <- int(i);
				}
			}
		}
	return tmp;
	}

	reflex move{
		//redémarrage et changement de cap en cas d'arrêt
		if(tour_balise!=0){
			write("tour_balise");
			tour_balise<-tour_balise-1;
		}
		//évitement de collision
		list<point> robot_point_collision;
		loop r over: robot at_distance distance_vue{
			add r.location to:robot_point_collision;
		}
		list<point> objet_point_collision;
		loop o over: obstacle at_distance distance_vue{
			add o.location to:objet_point_collision;
		}
		if(intersection (robot_point_collision, vitesse_robot) or intersection(objet_point_collision, 2.0)){
			write("collision");
			vitesse<-0.0;
		}
		//destruction d'une mine
		else if(!empty(mine at_distance 0)){
			write("mine");
			ask any (mine at_distance 0){
				do die;
			}
		}
		//repérage d'une mine
		else if(!empty(mine at_distance distance_vue)){
			write("destruction mine");
			mine a_detruire <- any (mine at_distance distance_vue);
			do goto target:a_detruire speed:vitesse;
		}
		//récupération d'une balise: faire demi-tour
		else if(!empty(balise at_distance 0) and tour_balise=0){
			write("recup balise");
			ask any (balise at_distance 0){
				do die;
			}
			nb_balise <- nb_balise + 1;
			if(angle>=180){
				angle <- angle-180;
			}
			else{
				angle <- angle+180;
			}
		}
		//repérage d'une balise
		else if(!empty(balise at_distance distance_vue) and tour_balise=0){
			write("repercage balise");
			balise b <- any(balise at_distance distance_vue);
			do goto target:b speed:vitesse;
		}
		//dépôt aléatoire de balise
		else if(rnd(99)<proba_depot_balise and nb_balise>0){
			write("depot balise");
			create balise with:[location::self.location];
			nb_balise<-nb_balise - 1;
			tour_balise<-int(distance_vue/vitesse)+2;
		}
		//mouvement de recherche
		else{
			///changement aléatoire de direction
			if(rnd(99)<proba_changement_cap){
				angle<-chgt_cap(angle);
			}
			write("recherche: " + speed + ";" + angle);
			do move speed:vitesse heading:angle;
		}
		if(vitesse=0.0){
			write("redémarrage");
			angle<-rnd(360);
			vitesse<- vitesse_robot;
		}
		float mvt_x<-vitesse*cos(angle);
		float mvt_y<-vitesse*sin(angle);
		//border
		if(self.location.x+mvt_x <= 0 or self.location.x+mvt_x >= world.shape.width or self.location.y+mvt_y <= 0 or self.location.y +mvt_y >= world.shape.height){
			write("bordure");
			if(angle>=180){
				angle <- angle-180;
			}
			else{
				angle <- angle+180;
			}
		}
	}
	aspect base{
		draw triangle(2) color:#red;
//		draw line([self.location, {self.location.x+vitesse*cos(angle), self.location.y+vitesse*sin(angle)}])color:#black;
//		draw circle(distance_vue) empty:true color:#red;
	}
}

species mine{
	aspect base{
		draw circle(2) color:#yellow;
	}
}

species obstacle{
	aspect base{
		draw circle(2) color:#gray;
	}
}

species balise{
	aspect base{
		draw circle(1) color:#red;
	}
}

experiment deminage{
	output{
		display planete{
			species zone_deploiement aspect:base;
			species robot aspect:base;
			species mine aspect:base;
			species balise aspect:base;
			species obstacle aspect:base;
		}
	}
}